#include "common.h"

#include <stdio.h>

int main(int argc, char **argv)
{
    printf("a: %d / b: %d\n",
            function_a(), function_b());
    return 0;
}
