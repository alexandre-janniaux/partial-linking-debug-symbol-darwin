CFLAGS = -g -gdwarf

all: show_info

liba.o: liba.c
libb.o: libb.c
main.o: main.c

libcommon.o: liba.o libb.o
	$(LINK.o) -o $@ $(CFLAGS) $(LDFLAGS) -r $^

libcommon.dylib: libcommon.o
	$(LINK.o) -shared -dylib -o $@ $(CFLAGS) $(LDFLAGS) $^

main: main.o | libcommon.dylib
main: LDFLAGS+=-lcommon

show_info: main
	dwarfdump --regex --name function liba.o
	dwarfdump --regex --name function libb.o
	dwarfdump --regex --name function libcommon.o
	dwarfdump --regex --name function libcommon.o
	dwarfdump --regex --name function main

clean:
	rm liba.o libb.o main.o libcommon.o main
